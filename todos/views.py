from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.
def todos(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_objects": todo,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            todolist.save()
            return redirect("todo_list_detail", id=todolist.id)

    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method =="POST":
        form = TodoForm(request.POST, instance = todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance = todo)

        context = {
            "todo_object": todo,
            "todo_form": form,
        }
        return render(request, "todos/update.html", context)

def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
                todoitem = form.save()
                return redirect("todo_list_detail", id=todoitem.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/itemscreate.html", context)

def todo_item_update(request, id):
    todo = TodoItem.objects.get(id=id)
    if request.method =="POST":
        form = TodoItemForm(request.POST, instance = todo)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.list.id)
    else:
        form = TodoItemForm(instance = todo)

        context = {
            "todo_item_form": form,
        }
        return render(request, "todos/todoitemupdate.html", context)
